CREATE DATABASE question DEFAULT CHARACTER SET utf8;

USE question;

CREATE TABLE item_category(
	category_id int PRIMARY KEY AUTO_INCREMENT,
	category_name varchar(256) NOT NULL
);
